# service-center

`service-center`模块用于部署注册中心。

## 介绍
- 此文档将使用Helm包管理器在Kubernetes上引导您安装`service-center`服务。
- 使用`helm`包管理器可以将服务安装在`k8s`集群上。
- `chart`包中的文件列表如下：

```
service-center/
	|- templates/
        |- service-center-configmap.yaml
        |- service-center-deployment.yaml
        |- service-center-service.yaml
	|- .helmignore
	|- Chart.yaml
	|- values.yaml
	|- README.md
```


## 环境准备

- Kubernetes 1.16+
- Helm 3+

> Helm和Kubernetes有严格的版本支持策略，点击查看[Helm版本支持策略](https://helm.sh/zh/docs/topics/version_skew/)

## 安装

使用`helm`安装`service-center`服务：
```
$ helm install service-center service-center
```


## 卸载

卸载或者删除`service-center`资源：
```
$ helm uninstall service-center
```

## 参数
下表包含的是`chart`中所有的自定义参数

### 通用参数
| 参数                    | 描述        | 默认值 |
|-----------------------| ----------- | ------ |
| global.ssl.enabled    | 是否启用ssl | false  |
| global.ssl.secretName | ssl秘钥名称 |        |


### 镜像参数
| 参数             | 描述     | 默认值                                                    |
| ---------------- | -------- | --------------------------------------------------------- |
| images.repository | 镜像地址 | swr.cn-north-4.myhuaweicloud.com/eg-common/service-center |
| images.tag        | 镜像标签 | latest                                                    |
| images.pullPolicy | 拉取策略 | IfNotPresent                                              |