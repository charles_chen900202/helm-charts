# Edgegallery Helm Chart
[![许可证](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

部署您自己的私人 Edgegallery。

## 前置条件
* Kubernetes 1.6+
* Helm 3
* [如果启用] nfs 服务器和对它的 RW 访问
* [如果启用] nfs-client-provisioner 用于动态配置

```
helm install nfs-client-provisioner --set nfs.server=<nfs_sever_ip> --set nfs.path=<nfs_server_directory> stable/nfs-client-provisioner
```

* [如果启用] nginx-ingress-controller for ingress

```
kubectl label <node_name> node=edge
helm install nginx-ingress-controller stable/nginx-ingress --set controller.kind=DaemonSet --set controller.nodeSelector.node=edge --set controller.hostNetwork=true
```

## Edgegallery 安装（在线）

1. 项目克隆

```shell
$ git clone https://gitee.com/edgegallery/helm-charts.git
```

2. 按需安装模块

```shell
## 示例
$ cd service-center
$ helm install my-service-center . 
```




## Edgegallery 安装（离线）
* 安装指南：[链接](https://gitee.com/edgegallery/installer/blob/Release-v1.5/ansible_install/README-cn.md)
